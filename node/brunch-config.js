"use strict";

module.exports = {
    files: {
        javascripts: {
            joinTo: 'js/app.js'
        },
        stylesheets: {
            joinTo: "css/app.css"
        }
    },

    conventions: {
        assets: /^(static)/
    },

    paths: {
        watched: ["static", "css", "js", "vendor"],
        public: "../assets"
    }
};
