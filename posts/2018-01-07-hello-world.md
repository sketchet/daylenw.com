---
title: Hello, World!
date: 2018-01-07 00:00:00
---


## Welcome

Welcome to my blog. I don't plan on posting often but occasionally
I will write about my musings.


## Serum

I built this website with the awesome project
[Serum](http://dalgona.hontou.moe/Serum/index.html). Serum is a static
website generator written in the Elixir programming language. It was
dead simple to setup and start writing.


## Adios!

Well, now I know everything is working. Until next time!
